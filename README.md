# DPTS Characterisation

This project contains python scripts that can be used for Digital Pixel Test Structure (DPTS) Characterisation. It contains two main scripts. The first one was used to make a calibration map for an O-variant detector, the second for an X-variant.